const fs = require('fs');
const https = require('https');
var path = require('path');

const options = {
  key: fs.readFileSync('sslcert/key.pem', 'utf8'),
  cert: fs.readFileSync('sslcert/cert.pem', 'utf8'),
  passphrase: process.env.HTTPS_PASSPHRASE || ''
};

https.createServer(options, function (request, response) {
    var filePath = '.' + request.url;
    if (filePath == './')
        filePath = './index.html';

    var extname = path.extname(filePath);
    var contentType = 'text/html';
    switch (extname) {
        case '.js':
            contentType = 'text/javascript';
            break;
    }

    fs.readFile(filePath, function(error, content) {
        if (error) {
            if(error.code == 'ENOENT'){
                fs.readFile('./404.html', function(error, content) {
                    response.writeHead(200, { 'Content-Type': contentType });
                    response.end(content, 'utf-8');
                });
            }
            else {
                response.writeHead(500);
                response.end('Sorry, check with the site admin for error: '+error.code+' ..\n');
                response.end(); 
            }
        }
        else {
            response.writeHead(200, { 'Content-Type': contentType });
            response.end(content, 'utf-8');
        }
    });

}).listen(8080);
console.log("Listening on https://localhost:8080");
