# Google Pay Integration POC

## How to Use

This is a self-contained POC that is an example of how we could integrate Google Pay payments in Avon Shop or ePayment. Run the application from command line by typing the following:

```Bash
$ node server.js
```

Then open the page in your browser: https://localhost:8080/.
![The payment button](images/step1.png)

Click the payment button, select the payment method in the popup that appears. If you do not have a card registered with Google Pay, you can enter it there. Similarly, if you do not have an address, you will have to enter it.
![The payment popup](images/step2.png)
![Add new card number](images/step3.png)

If the payment is successful, you will get a token displayed on the page. This token has to be sent to Apexx for processing.

## The Source Code

The application contains an `sslcert` folder that contains the certificate and private key that is required for HTTPS. The `server.js` is a simple web server implementation. The `index.html` file contains the javascript that takes care of the integration. The `getTransactionInfo()` function returns the amount to pay and the currency, so that should be changed obviously. The token is available in the `onGooglePaymentsButtonClicked()` function.